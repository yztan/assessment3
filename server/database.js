var Sequelize = require('sequelize');
var config = require("./config");

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'password1234';

var database = new Sequelize('assess3', MYSQL_USERNAME, MYSQL_PASSWORD,
    {
        dialect: "mysql",
        logging: console.log,
         pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var User = require("./model/User.js");

database
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});

// database
// .sync({force: config.seed})
// .then(function () {
//     console.log("Database in Sync Now");
//     require("./seed")();
// });

module.exports = database