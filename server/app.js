// application
var express = require('express');
var setupPassport = require('./passport');
var flash = require('connect-flash');
var appRouter = require('./routers/appRouter.js')(express);
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var jsonParser = bodyParser.json();
var path = require('path');
var user=require('./controllers/signupController.js');


var port = process.env.PORT || 3000
var app = express();
app.use(express.static(path.join(__dirname, "../client")));


app.use(cookieParser());
app.use(session({ 
    secret: '4564f6s4fdsfdfd', 
    resave: false, 
    saveUninitialized: false,
    cookie: {
        expires: 60000
    } 
}));

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});

app.use(flash())
app.use(function(req, res, next) {
res.locals.errorMessage = req.flash('error')
next()
});


app.use(jsonParser)
app.use(bodyParser.urlencoded({extended: true }))
app.post('/signup',user.signup);

setupPassport(app)
app.use('/', appRouter)

// start app
app.listen(port, function() {
    console.log("Application started on port %d", port);
});

module.exports.getApp = app