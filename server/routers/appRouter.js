var passport = require('passport'),
signupController = require('../controllers/signupController.js')
const API_USERS_URI = "/api/users";

module.exports = function(express,database) {
    var router = express.Router()

      var isAuthenticated = function (req, res, next) {
      if (req.isAuthenticated())
        return next()
      req.flash('error', 'You have to be logged in to access the page.')
      res.redirect('/')
      }

      router.post('/signup', signupController.signup);
      router.get(API_USERS_URI, isAuthenticated, signupController.list);
      router.post(API_USERS_URI, isAuthenticated, signupController.create);
      router.post(API_USERS_URI + '/:id', isAuthenticated, signupController.update);
      router.delete(API_USERS_URI + '/:id', isAuthenticated, signupController.remove);




      router.post('/login', passport.authenticate('local', {
        successRedirect: '/home',
        failureRedirect: '/',
        failureFlash: true 
      }))

      router.get('/logout', function(req, res) {
      req.logout()
      res.redirect('/')
      })
      return router
      }