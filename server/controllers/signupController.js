var bcrypt = require('bcrypt'),
Model = require('../model/models.js')

exports.signup = function(req, res) {
var username = req.body.username
var password = req.body.password
var password2 = req.body.password2
var email = req.body.email

if (!username || !password || !password2 || email) {
req.flash('error', "Please, fill in all the fields.")
res.redirect('#!/signup')
}

if (password !== password2) {
req.flash('error', "Please, enter the same password twice.")
res.redirect('#!/signup')
}

var salt = bcrypt.genSaltSync(10)
var hashedPassword = bcrypt.hashSync(password, salt)

var newUser = {
username: username,
email: email,
salt: salt,
password: hashedPassword,

}

Model.User.create(newUser).then(function() {
res.redirect('#!/home')
}).catch(function(error) {
req.flash('error', "Please, choose a different username.")
res.redirect('#!/signup')
})
}

exports.login=function(req,res,next){
    var email=req.body.email;
    var password=req.body.password;

    User.findOne({email:email}, function(err,user){
      if(user==null){
        res.status(400).end('No account with this email');
      }
     else{
      req.body.username=user.username;
      user.comparePassword(password,function(err,isMatch){
       if(isMatch && isMatch==true){
         next();
       }else{
         res.status(400).end('Invalid email or password');
       }
     });
     }
    });
}

exports.list = function (req, res) {
  User
      .findAll()
      .then(function (users) {
          res.json(users);
      })
      .catch(function (err) {
          handleErr(res, err);
      });
};
exports.create = function (req, res) {
  User
      .create(req.body)
      .then(function (user) {
          res.json(user);
      })
      .catch(function (err) {
          handleErr(res, err);
      });
};

exports.update = function (req, res) {
  User
      .findById(req.params.id)
      .then(function (user) {

          if (!user) {
              handler404(res);
          }

          res.json(user);
      })
      .catch(function (err) {
          handleErr(res, err);
      });
};

exports.remove = function (req, res) {
  User
      .findById(req.params.id)
      .then(function (user) {
          if (!user) {
              handler404(res);
          }

          res.json(user);
      })
      .catch(function (err) {
          handleErr(res, err);
      });
};


function handleErr(res) {
  handleErr(res, null);
}


function handleErr(res, err) {
  console.log(err);
  res
      .status(500)
      .json({
          error: true
      });
}

function handler404(res) {
  res
      .status(404)
      .json({message: "User not found!"});
}

function returnResults(results, res) {
  res.send(results);
}