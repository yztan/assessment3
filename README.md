# Gig Kit Project

### Getting the code up and running
1. Firstly you will need to clone this repository by running the ```git clone <project's Github URL>``` command
1. After you've that you'll need to make sure that you have **npm** and **bower** installed
1. You can get **npm** by installing Node from [here](https://nodejs.org/en/)
1. Once you've done this you'll need to run the following command:
     `npm install -g bower # this may require sudo on Mac/Linux`
1. Once **npm** and **bower** are installed, you'll need to install all of the dependencies in *package.json* and *bower.json*
  ```
  npm install
 
  bower install
  ```

The signup/Authentication not working. Skip and go to localhost:3000/#!/home to see the rest of the pages

Only the signup works via postman and keying  http://localhost:3000/signup



