(function(){

angular.module('BandSite').config(function($routeProvider)
{
    
       $routeProvider
       .when('/', {
           templateUrl: 'templates/index.html',
           controller: ''
       })
       .when('/home', {
           templateUrl: 'templates/home.html',
           controller: 'HomeController'
       })
       .when('/list', {
           templateUrl: 'templates/list.html',
           controller: 'HomeController'
       })
       .when('/profile', {
           templateUrl: 'templates/profile.html',
           controller: 'HomeController'
       })
       .when('/band', {
           templateUrl: 'templates/band.html',
           controller: 'BandController'
       })
       .when('/gigs', {
           templateUrl: 'templates/gigs.html',
           controller: 'GigsController'
       })
       .when('/signup', {
           templateUrl: 'templates/signup.html',
           controller: 'RegCtrl'
       })
       .when('/selectable', {
           templateUrl: 'templates/selectable.html',
           controller: 'SelectController'
       })
       .when('/contact', {
           templateUrl: 'templates/contact.html',
           controller: 'ContactController'
       })
       .when('/login', {
           templateUrl: 'templates/login.html',
           controller: 'LoginController'
       })

   
    }
)})();
