(function(){
    angular
    .module("BandSite")
    .controller('GigsController', function($scope, $sce){
        
                $scope.oneAtATime = true;
        
                    // List of gigs and venues etc
                  $scope.groups = [
                    {
                          date: '01/11/17',
                          city: 'Singapore',
                        venue: 'Timbre+',
                        details: "Live Band needed",
                        compensation: "$50/hr",
                        duration: "2hours",
                          map: $sce.trustAsResourceUrl("https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7954537830014!2d103.78530974981366!3d1.2974177620996872!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1a4fdcdcb793%3A0x212cfcf8b61862cd!2sTimbre%2B!5e0!3m2!1sen!2ssg!4v1506695892407"),
                          price: 12.5
                    },
                    {
                          date: '02/11/17',
                          city: 'Singapore',
                        venue: 'Beer Market',
                        details: "DJ needed",
                        compensation: "$50/hr",
                        duration: "2hours",
                          map: $sce.trustAsResourceUrl(
                            "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8052060384675!2d103.84453529109179!3d1.291217739891137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19a03bf2fb21%3A0x62de619866e0271b!2sBeer+Market!5e0!3m2!1sen!2ssg!4v1506696514531" ),
                        price: 10.5		   
                   },
                    {
                          date: '03/11/17',
                          city: 'Singapore',
                        venue: 'Esplanade Theatres',
                        details: "Singer needed",
                        compensation: "$50/hr",
                        duration: "2hours",
                          map: $sce.trustAsResourceUrl("https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8074314355354!2d103.85362254981365!3d1.289798762121382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19a7ec1ae549%3A0xb955a5f90a365835!2sEsplanade+-+Theatres+on+the+Bay%2C+Singapore!5e0!3m2!1sen!2ssg!4v1506696633958"),
                        price: 13
                    }
                  ];
        
            })}
        )();