
(function(){
angular
.module("BandSite")
    .controller('HomeController', function($scope, $sce) {
            // Function to loop through background-images
            function imageLoop (){
                $("#rotatorContainer div").last().fadeOut(1000,function(){
                    $(this).insertBefore($("#rotatorContainer div").first()).show();
                });
            }
    
            setInterval(function(){
                imageLoop();
            }, 5000);
    
            $scope.nextGigDate = '01/11/17'
            $scope.nextGigCity = 'Singapore',
            $scope.nextGigVenue = 'Timbre+',
            $scope.nextGigMap = $sce.trustAsResourceUrl(
                "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7954537830014!2d103.78530974981366!3d1.2974177620996872!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1a4fdcdcb793%3A0x212cfcf8b61862cd!2sTimbre%2B!5e0!3m2!1sen!2ssg!4v1506695892407"),
            $scope.nextGigPrice = 12.5
            
        })})();