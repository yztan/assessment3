(function(){
    angular
    .module("BandSite").controller('LogoutController',function(UserService,$scope,$location){

  $scope.logout=function(){
    UserService.logout();
    $location.path('index');
  
  }

})
})();
