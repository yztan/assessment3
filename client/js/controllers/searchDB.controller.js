(function(){
    angular
    .module("BandSite")
    .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$state', 'UserService'];

    function SearchDBCtrl($state, UserService) {
        UserServices.getAllProfiles().then(function(result){
            vm.sociallogins = result.data;
        });

    }
})();