(function () {
    'use strict';
    angular
        .module('BandSite')
        .controller('RegCtrl',['UserService',function(UserService,$scope,$state){
           
            $scope.user={};
                        
            $scope.signup=function(){
              $scope.signupError=null;
              if($scope.user.password2 !== $scope.user.password){
                  $scope.signupError='Opps! password did not match, type again';
                  $scope.user.password2="";
                  return;
              }
              var request_body={"username":$scope.user.username,"email":$scope.user.email,"password":$scope.user.password};
              UserService.signup(request_body)
              .then(function(response){
                    
                     setTimeout(function(){$state.path('#!/login');},3000) ;
                    }
                   ,function(error){$scope.signupError='An account with same username or email already exist';}
                 );
            }
        }]);
})();